## 前言

整改内容：等保测评整改

- linux centos7配置口令复杂度和有效期策略
- 在服务器中配置口令复杂度策略:如密码由至少1位大小写字母、数字、特殊字符组成,口令有效期为90天。
- 在服务器中配置登录失败5次锁定账户3分钟,超时退出15分钟。

## 修改密码复杂度设置

- 编辑PAM配置文件/etc/pam.d/system-auth，加入以下行：

- ```shell
  password requisite pam_cracklib.so try_first_pass retry=3 minlen=8 ucredit=-1 lcredit=-2 dcredit=-1 ocredit=-1
  ```

- 解释：

  - pam_cracklib.so 是一个密码强度检查模块。
  - try_first_pass 表示优先使用前面已经输入的密码。
  - retry=3 表示输入密码最多尝试3次。
  - minlen=8 表示密码长度至少为8个字符。
  - ucredit=-1 表示密码中至少要有1个大写字母。
  - lcredit=-2 表示密码中至少要有2个小写字母。
  - dcredit=-1 表示密码中至少要有1个数字。
  - ocredit=-1 表示密码中至少要有1个特殊字符（如 @、#、$ 等）。

- 保存文件后，重新启动服务器或重新加载PAM模块以使更改生效。

- ```shell
  [root@localhost ~]# cat /etc/pam.d/system-auth
  #%PAM-1.0
  # This file is auto-generated.
  # User changes will be destroyed the next time authconfig is run.
  auth        required      pam_env.so
  auth        required      pam_faildelay.so delay=2000000
  auth        sufficient    pam_unix.so nullok try_first_pass
  auth        requisite     pam_succeed_if.so uid >= 1000 quiet_success
  auth        required      pam_deny.so

  # 配置登录失败锁定账户策略
  auth        required      pam_tally2.so deny=5 unlock_time=180
  # 配置登录失败锁定账户策略

  account     required      pam_unix.so
  account     sufficient    pam_localuser.so
  account     sufficient    pam_succeed_if.so uid < 1000 quiet
  account     required      pam_permit.so

  password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=

  # 修改密码复杂度设置
  password    requisite     pam_cracklib.so try_first_pass retry=3 minlen=8 ucredit=-1 lcredit=-2 dcredit=-1 ocredit=-1
  # 修改密码复杂度设置

  password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
  password    required      pam_deny.so

  session     optional      pam_keyinit.so revoke
  session     required      pam_limits.so
  -session     optional      pam_systemd.so
  session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
  session     required      pam_unix.so

  ```



## 设置密码有效期

- 编辑密码策略文件/etc/login.defs，在其中加入以下行：

- ```shell
  PASS_MAX_DAYS 90
  PASS_MIN_DAYS 0
  PASS_WARN_AGE 14
  ```

- 解释：

  - PASS_MAX_DAYS 90 表示密码的最长有效期为90天。
  - PASS_MIN_DAYS 0 表示可以在任何时候更改密码。
  - PASS_WARN_AGE 14 表示在密码过期之前14天发出警告。
  - 保存文件后，重新启动服务器或重新加载PAM模块以使更改生效。

- ```shell
  [root@localhost ~]# cat /etc/login.defs 
  #
  # Please note that the parameters in this configuration file control the
  # behavior of the tools from the shadow-utils component. None of these
  # tools uses the PAM mechanism, and the utilities that use PAM (such as the
  # passwd command) should therefore be configured elsewhere. Refer to
  # /etc/pam.d/system-auth for more information.
  #

  # *REQUIRED*
  #   Directory where mailboxes reside, _or_ name of file, relative to the
  #   home directory.  If you _do_ define both, MAIL_DIR takes precedence.
  #   QMAIL_DIR is for Qmail
  #
  #QMAIL_DIR	Maildir
  MAIL_DIR	/var/spool/mail
  #MAIL_FILE	.mail

  # Password aging controls:
  #
  #	PASS_MAX_DAYS	Maximum number of days a password may be used.
  #	PASS_MIN_DAYS	Minimum number of days allowed between password changes.
  #	PASS_MIN_LEN	Minimum acceptable password length.
  #	PASS_WARN_AGE	Number of days warning given before a password expires.
  #
  # 设置密码有效期为90天
  PASS_MAX_DAYS	90
  # 设置密码最小使用期限为7天
  PASS_MIN_DAYS	0
  # 设置密码最小长度为8位
  PASS_MIN_LEN	8
  # 设置密码过期前的警告期限为7天
  PASS_WARN_AGE	7

  #
  # Min/max values for automatic uid selection in useradd
  #
  UID_MIN                  1000
  UID_MAX                 60000
  # System accounts
  SYS_UID_MIN               201
  SYS_UID_MAX               999

  #
  # Min/max values for automatic gid selection in groupadd
  #
  GID_MIN                  1000
  GID_MAX                 60000
  # System accounts
  SYS_GID_MIN               201
  SYS_GID_MAX               999

  #
  # If defined, this command is run when removing a user.
  # It should remove any at/cron/print jobs etc. owned by
  # the user to be removed (passed as the first argument).
  #
  #USERDEL_CMD	/usr/sbin/userdel_local

  #
  # If useradd should create home directories for users by default
  # On RH systems, we do. This option is overridden with the -m flag on
  # useradd command line.
  #
  CREATE_HOME	yes

  # The permission mask is initialized to this value. If not specified, 
  # the permission mask will be initialized to 022.
  UMASK           077

  # This enables userdel to remove user groups if no members exist.
  #
  USERGROUPS_ENAB yes

  # Use SHA512 to encrypt password.
  ENCRYPT_METHOD SHA512
  ```

## 配置登录失败锁定账户策略

- 编辑PAM配置文件/etc/pam.d/system-auth，在其中加入以下行：

- ```shell
  auth required pam_tally2.so deny=5 unlock_time=180
  ```

- 解释：

  - pam_tally2.so 是一个帐户访问控制和计数模块。
  - deny=5 表示当有5次登录失败时，账户被锁定。
  - unlock_time=180 表示账户在被锁定后，需要等待180秒（3分钟）才能再次登录。
  - 保存文件后，重新启动服务器或重新加载PAM模块以使更改生效。

- ```shell
  [root@localhost ~]# cat /etc/pam.d/system-auth
  #%PAM-1.0
  # This file is auto-generated.
  # User changes will be destroyed the next time authconfig is run.
  auth        required      pam_env.so
  auth        required      pam_faildelay.so delay=2000000
  auth        sufficient    pam_unix.so nullok try_first_pass
  auth        requisite     pam_succeed_if.so uid >= 1000 quiet_success
  auth        required      pam_deny.so

  # 配置登录失败锁定账户策略
  auth        required      pam_tally2.so deny=5 unlock_time=180
  # 配置登录失败锁定账户策略

  account     required      pam_unix.so
  account     sufficient    pam_localuser.so
  account     sufficient    pam_succeed_if.so uid < 1000 quiet
  account     required      pam_permit.so

  password    requisite     pam_pwquality.so try_first_pass local_users_only retry=3 authtok_type=

  # 修改密码复杂度设置
  password    requisite     pam_cracklib.so try_first_pass retry=3 minlen=8 ucredit=-1 lcredit=-2 dcredit=-1 ocredit=-1
  # 修改密码复杂度设置

  password    sufficient    pam_unix.so sha512 shadow nullok try_first_pass use_authtok
  password    required      pam_deny.so

  session     optional      pam_keyinit.so revoke
  session     required      pam_limits.so
  -session     optional      pam_systemd.so
  session     [success=1 default=ignore] pam_succeed_if.so service in crond quiet use_uid
  session     required      pam_unix.so

  ```

## 配置超时退出策略

- 编辑[SSH服务](https://so.csdn.net/so/search?q=SSH%E6%9C%8D%E5%8A%A1&spm=1001.2101.3001.7020)配置文件/etc/ssh/sshd_config，在其中加入以下行：

- ```shell
  ClientAliveInterval 900
  ClientAliveCountMax 0
  ```

- 解释：

  - ClientAliveInterval 900 表示SSH客户端将每900秒（15分钟）发送一次保持活动消息
  - ClientAliveCountMax 0 表示保持活动消息的数量没有限制。
  - 保存文件后，重新启动SSH服务以使更改生效。

