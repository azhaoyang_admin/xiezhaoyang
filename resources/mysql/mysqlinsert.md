## CentOS7 安装MySQL(手动)

### 一、卸载MariaDB

- 在CentOS中默认安装有MariaDB，是MySQL的一个分支，主要由开源社区维护。

- CentOS 7及以上版本已经不再使用MySQL数据库，而是使用MariaDB数据库。

- 如果直接安装MySQL，会和MariaDB的文件冲突。

- 因此，需要先卸载自带的MariaDB，再安装MySQL。

- ```shell
  # 1.1 查看版本：
  rpm -qa|grep mariadb
  
  
  # 1.2 卸载
  rpm -e --nodeps 文件名
  # 1.3 检查是否卸载干净：
  rpm -qa|grep mariadb
  ```


- ![](../images/mysql/d86f3bf8ebbf4ed5826f49f00460290b.png)

- 删除mysql相关文件夹


  - ```shell
    # 删除mysql相关文件
    [root@VM-16-8-centos xiezhaoyang]# find / -name mysql
    /etc/selinux/targeted/active/modules/100/mysql
    /usr/lib64/mysql
    [root@VM-16-8-centos xiezhaoyang]# rm -rf /etc/selinux/targeted/active/modules/100/mysql
    [root@VM-16-8-centos xiezhaoyang]# rm -rf /usr/lib64/mysql
    # 删除mysql.sock
    [root@VM-16-8-centos xiezhaoyang]# find / -name mysql.sock
    # 删除mysqld.log
    [root@VM-16-8-centos xiezhaoyang]# find / -name mysqld.log
    # 删除my.cnf
    [root@VM-16-8-centos xiezhaoyang]# find / -name my.cnf
    ```



### 二、安装MySQL

#### 2.1 下载资源包

##### 2.1.1 官网下载

- MySQL官网下载地址 : [MySQL官网下载地址](https://dev.mysql.com/downloads/mysql/)
- ![](../images/mysql/19b0ecbd2ffe4a33b8838a0c26ecd172.png)

##### 2.1.2 wget下载

```shell
wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz
```

#### 2.2 解压

```shell
.tar.gz后缀：tar -zxvf 文件名
.tar.xz后缀：tar -Jxvf 文件名
```

#### 2.3 重命名

```shell
# 将解压后的文件夹重命名（或者为文件夹创建软链接）
# 重命名
mv 原文件夹名 mysql8
# 软链接
ln -s 文件夹名 mysql8
```

#### 2.4 添加PATH变量

- 添加PATH变量后，可在全局使用MySQL。

- 有两种添加方式：export命令临时生效、修改配置文件用久生效；

- ```shell
  #临时环境变量，关闭shell后失效，通常用于测试环境
  export PATH=$PATH:/data/software/mysql8/bin
  ```

### 三、用户和用户组

#### 3.1 创建用户组和用户

```shell
# 创建一个用户组：mysql
groupadd mysql

# 创建一个系统用户：mysql，指定用户组为mysql
useradd -r -g mysql mysql
```

- 创建用户组：groupadd

- 创建用户：useradd
    - -r：创建系统用户
    - -g：指定用户组

#### 3.2 数据目录

#### 3.2.1 创建目录

```shell
mkdir -p /data/software/mysql8/datas
```

#### 3.2.2 赋予权限

```shell
# 更改属主和数组
chown -R mysql:mysql /data/software/mysql8/datas

# 更改模式
chmod -R 750 /data/software/mysql8/datas
```

### 四、初始化MySQL

#### 4.1 配置参数

> 在/data/software/mysql8/下，创建my.cnf配置文件，用于初始化MySQL数据库

```mysql
[mysql]
# 默认字符集
default-character-set=utf8mb4
[client]
port       = 3306
socket     = /tmp/mysql.sock

[mysqld]
port       = 3306
server-id  = 3306
user       = mysql
socket     = /tmp/mysql.sock
# 安装目录
basedir    = /data/software/mysql8
# 数据存放目录
datadir    = /data/software/mysql8/datas/mysql
log-bin    = /data/software/mysql8/datas/mysql/mysql-bin
innodb_data_home_dir      =/data/software/mysql8/datas/mysql
innodb_log_group_home_dir =/data/software/mysql8/datas/mysql
#日志及进程数据的存放目录
log-error =/data/software/mysql8/datas/mysql/mysql.log
pid-file  =/data/software/mysql8/datas/mysql/mysql.pid
# 服务端使用的字符集默认为8比特编码
character-set-server=utf8mb4
lower_case_table_names=1
autocommit =1
 
 ##################以上要修改的########################
skip-external-locking
key_buffer_size = 256M
max_allowed_packet = 1M
table_open_cache = 1024
sort_buffer_size = 4M
net_buffer_length = 8K
read_buffer_size = 4M
read_rnd_buffer_size = 512K
myisam_sort_buffer_size = 64M
thread_cache_size = 128
  
#query_cache_size = 128M
tmp_table_size = 128M
explicit_defaults_for_timestamp = true
max_connections = 500
max_connect_errors = 100
open_files_limit = 65535
   
binlog_format=mixed
    
binlog_expire_logs_seconds =864000
    
# 创建新表时将使用的默认存储引擎
default_storage_engine = InnoDB
innodb_data_file_path = ibdata1:10M:autoextend
innodb_buffer_pool_size = 1024M
innodb_log_file_size = 256M
innodb_log_buffer_size = 8M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
transaction-isolation=READ-COMMITTED
      
[mysqldump]
quick
max_allowed_packet = 16M
       
[myisamchk]
key_buffer_size = 256M
sort_buffer_size = 4M
read_buffer = 2M
write_buffer = 2M
        
[mysqlhotcopy]
interactive-timeout
```

#### 4.2 初始化

```shell
mysqld --defaults-file=/data/software/mysql8/my.cnf --basedir=/data/software/mysql8/ --datadir=/data/software/mysql8/datas/mysql --user=mysql --initialize-insecure
```

- 参数（重要）
    - defaults-file：指定配置文件（要放在–initialize 前面）
    - user： 指定用户
    - basedir：指定安装目录
    - datadir：指定初始化数据目录
    - intialize-insecure：初始化无密码

### 五、启动MySQL

> 查看 MySQL的 bin路径下，是否包含mysqld_safe，用于后台安全启动MySQL。

#### 5.1 启动服务

```shell
# 完整命令
/data/software/mysql8/bin/mysqld_safe --defaults-file=/data/software/mysql8/my.cnf &

# 添加PATH变量后的命令（省略bin目录的路径）
mysqld_safe --defaults-file=/data/software/mysql8/my.cnf &

#查看是否启动
ps -ef|grep mysql
```

#### 5.2 登录

```shell
# 无密码登录方式
/data/software/mysql8/bin/mysql -u root --skip-password

# 有密码登录方式（初始的随机密码在/data/mysql8_data/mysql/mysql.log下）
mysql -u root -p
password:随机密码
```

#### 5.3 修改密码

```shell
# 修改密码
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456';

# 刷新权限
FLUSH PRIVILEGES;
```

#### 5.4 设置允许远程登录

> 登录到mysql里执行

```shell
mysql> use mysql
mysql> update user set user.Host='%'where user.User='root';
mysql> flush privileges;
mysql> quit
```

#### 5.5 在Navicat上测试连接
