## 1.什么是spring?

> - spring是一个轻量级的ioc和aop框架,是为java提供基础性能服务的框架。
> - spring属于低侵入式设计,代码污染性低。
> - spring中的ioc机制,把容器的依赖关系交给spring来管理降低程序的耦合性。
> - spring中的aop技术,可以将一些通用操作,如权限,日志,事务等进行统一处理,实现代码更好的复用。

## 2.谈谈你对IOC的理解

> - ioc就是控制反转,即创建对象的控制权的转移
> - 以前创建对象的时机是由我们自己控制的,现在交给spring来管理。避免重复创建对象
> - spring在运行时根据配置文件中的bean，并通过反射帮助我们动态的创建对象,管理对象,调用对象的方法,以及传递参数等

## 3.谈谈你对DI的理解

> - DI依赖注入,既应用程序在运行时,根据ioc容器动态的注入所需要的外部资源

## 4.谈谈你对AOP的理解

> - AOP也叫做面向切面编程，他是一个编程范式，目的就是提高代码的模块性。
> - Spring AOP基于动态代理的方式实现，
>   - 如果是实现了接口的话就会使用 JDK 动态代理，
>   - 反之则使用 CGLIB 代理，
> - Spring中 AOP的应用主要体现在 事务、日志、异常处理等方面。
> - 通过在代码的前后做一些增强处理，可以实现对业务逻辑的隔离，提高代码的模块化能力，实现解耦。

## 5. Spring AOP 和 AspectJ AOP 有什么区别？

> - Spring AOP 基于动态代理实现，属于运行时增强。
> - AspectJ 则属于编译时增强
>   - 编译时织入：指增强的代码和源代码我们都有，直接使用AspectJ编译器编译就行了，编译之后生成一个新的类，他也会作为一个正常的Java类装载到JVM。
>   - 编译后织入：指代码已经被编译成class文件或者已经打成 jar包，这时候要增强的话,就是编译后织入，比如你依赖了第三方的类库，又想对他增强的话，就可以通过这种方式。
>   - 加载时织入：指在 JVM 加载类的时候进行织入。
> - 总结下来的话：就是Spring AOP只能在运行时织入，不需要单独编译，性能相比 AspectJ 编译织入的方式慢。
> - 而 AspectJ 只支持编译前后和类加载时织入，性能更好，功能更加强大。

## 6.SpringBean的生命周期说说？

> SpringBean 生命周期简单概括为4个阶段：
>
> - 实例化：创建一个Bean对象
> - 填充属性：为属性赋值
> - 初始化
>   - 如果实现了`xxxAware`接口，通过不同类型的Aware接口拿到Spring容器的资源
>   - 如果实现了BeanPostProcessor接口，则会回调该接口的`postProcessBeforeInitialzation`和`postProcessAfterInitialization`方法
>   - 如果配置了`init-method`方法，则会执行`init-method`配置的方法
> - 销毁
>   - 容器关闭后，如果Bean实现了`DisposableBean`接口，则会回调该接口的`destroy`方法
>   - 如果配置了`destroy-method`方法，则会执行`destroy-method`配置的方法

## 7.Spring事务传播机制与隔离级别

> - spring支持编程式事务管理和声明式事务管理两种方式：
>   - 编程式事务使用TransactionTemplate。
>     - 优点：代码块级别,颗粒度细
>     - 缺点：代码耦合度比较高
>   - 声明式事务使用Transactional也就是AOP。
>     - 优点：用法简单，解耦
>     - 缺点：方法级颗粒度,容易死锁。
> - Spring的事务传播行为：
>   - 传播行为说的是，当多个事务同时存在的时候，spring如何处理这些事务的行为。分为七种
>   - PROPAGATION_REQUIRED(默认)：当前没有事务创建新事务，如果存在则加入该事务
>   - PROPAGATION_SUPPORTS：当前没有事务以非事务执行，如果存在则加入该事务
>   - PROPAGATION_MANDATORY：当前没有事务抛出异常，如果存在则加入该事务
>   - PROPAGATION_REQUIRES_NEW：始终创建新事务。
>   - PROPAGATION_NOT_SUPPORTED：以非事务方式执行，如果存在事务，就把事务挂起。
>   - PROPAGATION_NEVER：以非事务方式执行，如果存在事务，抛出异常。
>   - PROPAGATION_NESTED：如果当前存在事务，则在嵌套事务内执行。如果当前没有事务，则按REQUIRED属性执行。
> - Spring中的隔离级别：
>   -  ISOLATION_DEFAULT(默认)：
>     - 使用[数据库](https://cloud.tencent.com/solution/database?from=10680)默认的事务隔离级别。
>   - ISOLATION_READ_UNCOMMITTED：读未提交，允许另外一个事务可以看到这个事务未提交的数据。
>   - ISOLATION_READ_COMMITTED：
>     - 读已提交，保证一个事务修改的数据提交后才能被另一事务读取，而且能看到该事务对已有记录的更新。
>   - ISOLATION_REPEATABLE_READ：
>     - 可重复读，保证一个事务修改的数据提交后才能被另一事务读取，但是不能看到该事务对已有记录的更新。
>   - ISOLATION_SERIALIZABLE：一个事务在执行的过程中完全看不到其他事务对数据库所做的更新。

## 8.Spring是怎么解决循环依赖的？

> - Spring解决循环依赖的前提条件为不完全是构造器方式的循环依赖并且必为单例。
>
> - Bean的生命周期中通过创建对象与填充属性才能创建一个完整的bean。
>
> - 
>
> - spring通过三级缓存解决循环依赖的问题
>
> - 第一级缓存：用来保存实例化、初始化都完成的对象
>
> - 第二级缓存：用来保存实例化完成，但是未初始化完成的对象
>
> - 第三级缓存：用来保存一个对象工厂，提供一个匿名内部类，用于创建二级缓存中的对象
>
> - ![img](https://pic3.zhimg.com/80/v2-dbda6a103e41cde4a54548386d6c1cc2_1440w.webp)
>
> - 假设A、B互相依赖。
>
> - ![img](https://pic1.zhimg.com/80/v2-f20f1e81c8011fd99f723127cacf7430_1440w.webp)
>
>   1. 创建对象A，实例化时把A对象放入三级缓存
>
>      ![img](https://pic4.zhimg.com/80/v2-b295374aff4235ee9c0538bf03856217_1440w.webp)
>
>   2. A注入属性时，发现依赖B，转而去实例化B
>
>   3. 同样创建对象B，注入属性时发现依赖A,此时从一级到三级中缓存查询A，从三级缓存通过对象工厂拿到A，把A放入二级缓存，同时删除三级缓存中的A，此时，B已经实例化并且初始化完成，把B放入一级缓存。
>
>      1. ![img](https://pic2.zhimg.com/80/v2-51cb735fc6c02c2225d2f38b9b5b6da9_1440w.webp)
>
>   4. 接着继续创建A，顺利从一级缓存拿到实例化且初始化完成的B对象，A对象创建也完成，删除二级缓存中的A，同时把A放入一级缓存
>
>   5. 最后，一级缓存中保存着实例化、初始化都完成的A、B对象
>
>      ![img](https://pic2.zhimg.com/80/v2-a1eaa5d9f8bec23e373dc1b9f86ddb25_1440w.webp)
>
> - 因此，由于把实例化和初始化的流程分开了，所以如果都是用构造器的话，就没法分离这个操作，所以都是构造器的话就无法解决循环依赖的问题了。



## 9. 为什么要三级缓存?二级不行吗？

> - 不可以，主要是为了生成代理对象。
> - 因为三级缓存中放的是生成具体对象的匿名内部类，他可以生成代理对象，也可以是普通的实例对象。
> - 使用三级缓存主要是为了保证不管什么时候使用的都是一个对象。
> - 假设只有二级缓存的情况，往二级缓存中放的显示一个普通的Bean对象，
> - `BeanPostProcessor`去生成代理对象之后，覆盖掉二级缓存中的普通Bean对象，那么多线程环境下可能取到的对象就不一致了。
>
> ![img](https://pic3.zhimg.com/80/v2-e4e9050787017c7272dc313f88af9382_1440w.webp)


## 10.SpringMVC运行原理？

> - 客户端请求提交到DispatcherServlet
> - 由DispatcherServlet控制器查询一个或多个HandlerMapping，找到处理请求的Controller
> - DispatcherServlet将请求提交到Controller
> - Controller调用业务逻辑处理后，返回ModelAndView
> - DispatcherServlet查询一个或多个ViewResoler视图解析器，找到ModelAndView指定的视图
> - 视图负责将结果显示到客户端

## 11.Spring Boot的优势是什么？

> - 独立运行,配置简单,自动配置,没有代码生成,不用配置XML,无需部署war文件

## 12.怎么创建一个 Spring Boot 项目

> - 继承spring-boot-starter-parent项目
> - 导入spring-boot-dependencies项目依赖

## 13.Spring Boot的核心配置文件有哪些 ,区别是什么？

> - application.yml用于SpringBoot项目的自动化配置。
> - bootstrap.yml有以下几个应用场景
    >   - 使用nacos时,需要在bootstrap.yml配置注册中心的信息；
>   - 一些固定的不能被覆盖的属性；
>   - 一些加密/解密的场景
> - bootstrap.yml 加载优先于application.yml

## 14.springboot核心注解是哪个?他主要由那几个注解组成

> - 核心注解@SpringBootApplication
> - 它包含：
    >   - 读取配置文件的@SpringBootConfiguration
>   - 实现自动配置的@EnableAutoConfiguration
>   - spring组件扫描的@ComponentScan

## 15.SpringBoot的自动配置是如何实现的?

## 16.SpringBootStarter的工作原理是什么？

> - 启动注解@SpringBootApplication。
> - 是由@Configuration，@EnableAutoConfiguration，@ComponentScan组成。
> - 其中@EnableAutoConfiguration是实现自动配置的入口。
> - 该注解又通过@Import注解导入了AutoConfigurationImportSelector。
> - 在该类中加载META-INF/spring.factories的配置信息。
> - 然后筛选出以EnableAutoConfiguration为key的数据。
> - 加载到IOC容器中,实现自动配置功能。
> - 总结：其实就是SpringBoot在启动的时候,按照约定去读取SpringBootStarter的配置信息,再根据配置信息对资源进行初始化,并注入到Spring容器中。这样SpringBoot启动完毕后,就已经准备好了一切资源,使用过程中直接注入对应Bean资源即可。

## 17.sprinboot项目优化

> - 如果项目比较大,类比较多采用@Componment指定扫包范围
> - 在启动时候设置初始内存和最大内存相同
> - 将SpringBoot内置的tomcat更改为undertow

## links
* [目录](<catelog.md>)
* 下一节: [mybatis](resources/interview/mybatis.md)
