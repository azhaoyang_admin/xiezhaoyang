## CentOS7 安装MySQL(脚本)MySQL多实例安装部署

### 参考博客



- https://blog.csdn.net/qq_45675449/article/details/118033608
- https://zhuanlan.zhihu.com/p/138420773
- https://blog.csdn.net/qq_45675449/article/details/118032222
- 集群mysql面试题 https://www.cnblogs.com/fengzheng/p/13401783.html 
- https://www.cnblogs.com/zccoming/p/13033064.html

### 安装包地址

- wget https://dev.mysql.com/get/Downloads/MySQL-8.0/mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz
- ![](../images/mysql/19b0ecbd2ffe4a33b8838a0c26ecd172.png)

### 创建配置脚本 configuration.sh

```shell
# mysql安装地址
mysqlpath=/data/software
# mysql安装包
package=mysql-8.0.20-linux-glibc2.12-x86_64.tar.xz
# mysql安装后的名称
mysqlname=mysql-8.0.20-linux-glibc2.12-x86_64
# mysql组和用户
group=mysql
user=mysql
# mysql端口
port=3306
```

### 创建安装脚本 mysql8_automatic.sh

```shell
#!/bin/bash
#安装
install(){
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                            安装 MySQL                             |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
source ./configuration.sh;

Mysqlpath=$mysqlpath;
Package=$package;
Mysqlname=$mysqlname;
Group=$group;
User=$user;

while :;do
    Port=""
    read -p "[请输入数据库端口]Enter Mysql Port: " Port
    if [ "${Port}" = "" ]; then
        echo "[数据库端口不能为空！]Error: Port can't be NULL!!"
    else
        break
    fi
done

#创建下载路径文件夹
if [ -d "${Mysqlpath}" ]; then
  while :;do
    delete=""
    read -p "检测到文件夹${Mysqlpath}已存在！是否重新创建文件夹：[y/n]" delete
    if [ "${delete}" = "y" ]; then
        command=`rm -rf ${Mysqlpath}`;
        break;
    else
        if [ "${delete}" = "n" ];then
          echo "将使用已有的文件夹${Mysqlpath}存放下载文件";
          break;
        else
          echo "指令错误！";
        fi
    fi
  done
fi


mkdir -p ${Mysqlpath};
if [ $? -ne 0 ]; then
  echo "创建文件夹${Mysqlpath}失败";
  exit 1;
else
  echo "创建文件夹${Mysqlpath}成功";
fi


tar -Jxvf ${Package} -C ${Mysqlpath};
if [ $? -ne 0 ]; then
  echo "解压文件${Package}失败";
  exit 1;
else
  echo "解压文件${Package}成功";
fi


echo "创建组:"$Group"和用户:"$User;
egrep "^$Group" /etc/group >& /dev/null
if [ $? -ne 0 ]; then
  groupadd $Group;
fi

egrep "^$User" /etc/passwd >& /dev/null
if [ $? -ne 0 ]; then
  useradd -r -g $Group $User;
fi


echo "创建文件夹:${Mysqlpath}/${Mysqlname}/${Port}/data";
mkdir -p ${Mysqlpath}/${Mysqlname}/${Port}/data;


echo "赋予${Group}/${User}文件夹${Mysqlpath}/${Mysqlname}/${Port}权限";
chown -R $Group:$User ${Mysqlpath}/${Mysqlname}/${Port};


echo "调整:${Mysqlpath}/${Mysqlname}/${Port}权限为750";
chmod -R 750 ${Mysqlpath}/${Mysqlname}/${Port};


echo "创建my.cnf地址:${Mysqlpath}/${Mysqlname}/${Port}/my.cnf";
cat >> ${Mysqlpath}/${Mysqlname}/${Port}/my.cnf << EOC
[mysql]
# 默认字符集
default-character-set=utf8mb4
[client]
port       = ${Port}
socket     = ${Mysqlpath}/${Mysqlname}/${Port}/mysql.sock

[mysqld]
port       = ${Port}
server-id  = ${Port}
user       = mysql
socket     = ${Mysqlpath}/${Mysqlname}/${Port}/mysql.sock
# 安装目录
basedir    = ${Mysqlpath}/${Mysqlname}
# 数据存放目录
datadir    = ${Mysqlpath}/${Mysqlname}/${Port}/data
log-bin    = ${Mysqlpath}/${Mysqlname}/${Port}/data/mysql-bin
innodb_data_home_dir      =${Mysqlpath}/${Mysqlname}/${Port}/data
innodb_log_group_home_dir =${Mysqlpath}/${Mysqlname}/${Port}/data
#日志及进程数据的存放目录
log-error =${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.log
pid-file  =${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.pid
# 服务端使用的字符集默认为8比特编码
character-set-server=utf8mb4
lower_case_table_names=1
autocommit =1

 ##################以上要修改的########################
skip-external-locking
key_buffer_size = 256M
max_allowed_packet = 1M
table_open_cache = 1024
sort_buffer_size = 4M
net_buffer_length = 8K
read_buffer_size = 4M
read_rnd_buffer_size = 512K
myisam_sort_buffer_size = 64M
thread_cache_size = 128

#query_cache_size = 128M
tmp_table_size = 128M
explicit_defaults_for_timestamp = true
max_connections = 500
max_connect_errors = 100
open_files_limit = 65535

binlog_format=mixed

binlog_expire_logs_seconds =864000

# 创建新表时将使用的默认存储引擎
default_storage_engine = InnoDB
innodb_data_file_path = ibdata1:10M:autoextend
innodb_buffer_pool_size = 1024M
innodb_log_file_size = 256M
innodb_log_buffer_size = 8M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
transaction-isolation=READ-COMMITTED

[mysqldump]
quick
max_allowed_packet = 16M

[myisamchk]
key_buffer_size = 256M
sort_buffer_size = 4M
read_buffer = 2M
write_buffer = 2M

[mysqlhotcopy]
interactive-timeout

EOC

${Mysqlpath}/${Mysqlname}/bin/mysqld --defaults-file=${Mysqlpath}/${Mysqlname}/${Port}/my.cnf --basedir=${Mysqlpath}/${Mysqlname}/ --datadir=${Mysqlpath}/${Mysqlname}/${Port}/data --user=${User} --initialize

if [ $? -ne 0 ]; then
  echo "初始化MYSQL失败";
  exit 1;
else
  echo "初始化MYSQL成功";
fi

cat ${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.log
}

#安装新的实例
installNewLiving(){
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                      安装新的实例 MySQL                             |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
source ./configuration.sh;
Mysqlpath=$mysqlpath;
Mysqlname=$mysqlname;
Group=$group;
User=$user;

while :;do
    Port=""
    read -p "[请输入数据库端口]Enter Mysql Port: " Port
    if [ "${Port}" = "" ]; then
        echo "[数据库端口不能为空！]Error: Port can't be NULL!!"
    else
        break
    fi
done


echo "创建文件夹:${Mysqlpath}/${Mysqlname}/${Port}/data";
mkdir -p ${Mysqlpath}/${Mysqlname}/${Port}/data;


echo "赋予${Group}/${User}文件夹${Mysqlpath}/${Mysqlname}/${Port}权限";
chown -R $Group:$User ${Mysqlpath}/${Mysqlname}/${Port};


echo "调整:${Mysqlpath}/${Mysqlname}/${Port}权限为750";
chmod -R 750 ${Mysqlpath}/${Mysqlname}/${Port};


echo "创建my.cnf地址:${Mysqlpath}/${Mysqlname}/${Port}/my.cnf";
cat >> ${Mysqlpath}/${Mysqlname}/${Port}/my.cnf << EOC
[mysql]
# 默认字符集
default-character-set=utf8mb4
[client]
port       = ${Port}
socket     = ${Mysqlpath}/${Mysqlname}/${Port}/mysql.sock

[mysqld]
port       = ${Port}
server-id  = ${Port}
user       = mysql
socket     = ${Mysqlpath}/${Mysqlname}/${Port}/mysql.sock
# 安装目录
basedir    = ${Mysqlpath}/${Mysqlname}
# 数据存放目录
datadir    = ${Mysqlpath}/${Mysqlname}/${Port}/data
log-bin    = ${Mysqlpath}/${Mysqlname}/${Port}/data/mysql-bin
innodb_data_home_dir      =${Mysqlpath}/${Mysqlname}/${Port}/data
innodb_log_group_home_dir =${Mysqlpath}/${Mysqlname}/${Port}/data
#日志及进程数据的存放目录
log-error =${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.log
pid-file  =${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.pid
# 服务端使用的字符集默认为8比特编码
character-set-server=utf8mb4
lower_case_table_names=1
autocommit =1

 ##################以上要修改的########################
skip-external-locking
key_buffer_size = 256M
max_allowed_packet = 1M
table_open_cache = 1024
sort_buffer_size = 4M
net_buffer_length = 8K
read_buffer_size = 4M
read_rnd_buffer_size = 512K
myisam_sort_buffer_size = 64M
thread_cache_size = 128

#query_cache_size = 128M
tmp_table_size = 128M
explicit_defaults_for_timestamp = true
max_connections = 500
max_connect_errors = 100
open_files_limit = 65535

binlog_format=mixed

binlog_expire_logs_seconds =864000

# 创建新表时将使用的默认存储引擎
default_storage_engine = InnoDB
innodb_data_file_path = ibdata1:10M:autoextend
innodb_buffer_pool_size = 1024M
innodb_log_file_size = 256M
innodb_log_buffer_size = 8M
innodb_flush_log_at_trx_commit = 1
innodb_lock_wait_timeout = 50
transaction-isolation=READ-COMMITTED

[mysqldump]
quick
max_allowed_packet = 16M

[myisamchk]
key_buffer_size = 256M
sort_buffer_size = 4M
read_buffer = 2M
write_buffer = 2M

[mysqlhotcopy]
interactive-timeout

EOC

${Mysqlpath}/${Mysqlname}/bin/mysqld --defaults-file=${Mysqlpath}/${Mysqlname}/${Port}/my.cnf --basedir=${Mysqlpath}/${Mysqlname}/ --datadir=${Mysqlpath}/${Mysqlname}/${Port}/data --user=${User} --initialize

if [ $? -ne 0 ]; then
  echo "初始化MYSQL失败";
  exit 1;
else
  echo "初始化MYSQL成功";
fi

cat ${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.log
}

#启动
start_mysql(){
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                      启动 MySQL                                    |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
source ./configuration.sh;
Mysqlpath=$mysqlpath;
Mysqlname=$mysqlname;

while :;do
    Port=""
    read -p "[请输入数据库端口]Enter Mysql Port: " Port
    if [ "${Port}" = "" ]; then
        echo "[数据库端口不能为空！]Error: Port can't be NULL!!"
    else
        break
    fi
done

echo "正在尝试启动mysql...";
nohup ${Mysqlpath}/${Mysqlname}/bin/mysqld_safe --defaults-file=${Mysqlpath}/${Mysqlname}/${Port}/my.cnf  >/dev/null 2>&1 &
if [ $? -ne 0 ]; then
  echo "启动MYSQL失败";
  exit 1;
else
  echo "启动MYSQL成功";
fi
}

#查看状态
status_mysql(){
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                      查看 MySQL 状态                               |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
ps -ef|grep mysql;
}

#关闭mysql
stop_mysql(){
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                      关闭 MySQL                                    |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
source ./configuration.sh;
Mysqlpath=$mysqlpath;
Mysqlname=$mysqlname;

#输入端口号
while :;do
    Port=""
    read -p "[请输入数据库端口]Enter Mysql Port: " Port
    if [ "${Port}" = "" ]; then
        echo "[数据库端口不能为空！]Error: Port can't be NULL!!"
    else
        break
    fi
done

#输入密码
while :;do
    Password=""
    read -p "[请输入数据库密码]Enter Mysql Password: " Password
    if [ "${Password}" = "" ]; then
        echo "[数据库密码不能为空！]Error: Password can't be NULL!!"
    else
        break
    fi
done

#${Mysqlpath}/${Mysqlname}/bin/mysqladmin -uroot -p -S /tmp/mysql.sock shutdown

#使用初始密码连接mysql并更改密码，root用户授权
${Mysqlpath}/${Mysqlname}/bin/mysql --connect-expired-password  -uroot -p"${Password}" -P${Port} -h127.0.0.1  >tables.txt  <<EOF
shutdown;
quit
EOF

}

#重置 MySQL root 密码
reset_mysql_root_password(){
source ./configuration.sh;
Mysqlpath=$mysqlpath;
Mysqlname=$mysqlname;
# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "must root"
    exit 1
fi
echo "+-------------------------------------------------------------------+"
echo "|                                                                   |"
echo "|                      重置 MySQL root 密码                         |"
echo "|                                                                   |"
echo "+-------------------------------------------------------------------+"
#输入端口号
while :;do
    Port=""
    read -p "[请输入数据库端口]Enter Mysql Port: " Port
    if [ "${Port}" = "" ]; then
        echo "[数据库端口不能为空！]Error: Port can't be NULL!!"
    else
        break
    fi
done
#数据库密码
while :;do
    DB_Root_Password=""
    read -p "[请输入数据库密码]Enter Mysql ${DB_Name} root password: " DB_Root_Password
    if [ "${DB_Root_Password}" = "" ]; then
        echo "[密码不能为空！]Error: Password can't be NULL!!"
    else
        break
    fi
done
#数据库新密码
while :;do
    DB_yuancheng_Password=""
    read -p "请输入新的数据库密码: " DB_yuancheng_Password
    if [ "${DB_yuancheng_Password}" = "" ]; then
        echo "[密码不能为空！]Error: Password can't be NULL!!"
    else
        break
    fi
done

mysql_passwd_origin=` cat ${Mysqlpath}/${Mysqlname}/${Port}/data/mysql.log | grep password | head -1 | rev  | cut -d ' ' -f 1 | rev`;
if [ $mysql_passwd_origin == $DB_Root_Password ];then
  sqlHost="localhost"
else
  sqlHost="%";
fi

echo $sqlHost
#使用初始密码连接mysql并更改密码，root用户授权
${Mysqlpath}/${Mysqlname}/bin/mysql --connect-expired-password  -uroot -p"${DB_Root_Password}" -P${Port} -h127.0.0.1  >tables.txt  <<EOF
ALTER USER 'root'@'${sqlHost}' IDENTIFIED WITH mysql_native_password BY '${DB_yuancheng_Password}';
flush privileges;
use mysql
update user set user.Host='%'where user.User='root';
flush privileges;
quit
EOF

echo "修改端口为${Port}的Mysql成功,当前密码:${DB_Root_Password},更改后的密码${DB_yuancheng_Password}"
}


echo "-------------------------一键安装/卸载/修改密码------------------------"
echo "安装前确保mysql卸载干净：[find / -name [mysql,mysql.sock,mysqld.log,my.cnf]]"
echo "此脚本为单节点多实例部署：采用不同的配置文件加载不同的数据库"
echo "请选择：(输入序号)"
echo "1.安装"
echo "2.安装新的实例"
echo "3.启动mysql"
echo "4.查看mysql状态"
echo "5.关闭mysql"
echo "6.修改密码"
echo "7.备份mysql"
echo "----------------------------------------------------------------------"

while :; do
	read ch
	case $ch in
	        1)
	                install;
	                break;
	                ;;
          2)
                  installNewLiving;
                  break;
                  ;;
	        3)
	                start_mysql;
	                break;
	                ;;
          4)
                  status_mysql;
                  break;
                  ;;
          5)
                  stop_mysql;
                  break;
                  ;;
	        6)
	               	reset_mysql_root_password;
	               	break;
	                ;;
	        *)
					echo "指令错误！"
					;;
	esac
done
```

### 脚本使用说明

- 需保证服务器mysql卸载干净

- 修改密码必须安装之后才能使用

- mysqld: error while loading shared libraries: libnuma.so.1: cannot open shared object file: No such file or directory

    - yum -y install numactl

- > -------------------------一键安装/卸载/修改密码------------------------
  > 请选择：(输入序号)
  > 1.安装
  > 2.启动mysql
  > 3.查看mysql状态
  > 4.关闭mysql
  > 5.修改密码


![](/images/20230414111350.png)

