## Collection子接口之->List接口

> - [ ] 鉴于Java中数组用来存储数据的局限性,通常使用List替代数组。
> - [ ] List特点元素有序、且可重复,集合中的每个元素都有其对应的顺序索引。
> - [ ] List中的元素都对应一个整数型的序号记载其在容器中的位置,可以根据序号存取容器中的元素。
> - [ ] 常用实现类有:ArrayList、LinkedList和Vector。

---

## List接口之->ArrayList

- 是大小可变数组的实现,存储在内的数据称为元素.ArrayList 中可不断添加元素，其大小也自动增长。

- 特点是增删慢,查询快.是最常用的集合。

- ArrayList不能存储基本类型,基本数据类型对应的包装类型可以。
- 

- 当我们new ArrayList()并且第一次调用.add()方法的时候,底层会创建了一个Object[] elementData 并且指定长度为10。

- 当我们再次调用.add的时候,如果此次的添加导致底层elementData数组容量不够,则扩容。

- 默认情况下，扩容为原来的容量的1.5倍，同时需要将原有数组中的数据复制到新的数组中。

---

## List接口之->LinkedList

- LinkedList:双向链表,内部没有声明数组,而是定义了Node类型的first和last,用于记录首尾元素。
- 同时,定义内部类Node,作为LinkedList中保存数据的基本结构。
- Node除了保存数据,还定义了两个变量：

  *   prev变量记录前一个元素的位置
  *   next变量记录下一个元素的位置

*   特点是增删快,效率较高

---

## List接口之->Vector

- Vector大多数操作与ArrayList相同,区别之处在于Vector是线程安全的。

---

## Collection子接口之->Set接口

- 无序性:不是随机性,是你添加数据的时候不能按照数组索引的顺序添加,而是根据哈希值决定的

- 不可重复性:保证添加的元素按照equals判断时,不能返回true。就是相同元素只能填一次

---

## Set接口之->HashSet

- 线程不安全，可以存储null值

- 当向HashSet中添加元素A时,调用元素A所在类的hashCode()方法,计算元素A的哈希值。

- 把元素A的哈希值按照某种算法来计算,从而算出元素A在HashSet底层数组中的所在位置(即为索引位置)。

- 然后判断数组对应索引位置上是否已经存在了其他元素。

- 如果此位置上没有其他元素,则元素A添加成功。

- 如果此位置上已有其他元素B(或者以链表形式存在的多个元素)时,则比较元素A与元素B的hash值

- 如果hash值不相同,则元素A添加成功。

- 如果hash值相同,进而需要调用元素A所在类的equlas()方法:

- 返回true**元素A**添加失败,

- 返回false,**元素A**添加成功。(**情况3**)

---

## Set接口之->LinkedHashSet

- LinkedHashSet是HashSet 的子类,在添加数据的同时,每个数据还维护了两个引用,记录此数据前一个数据和后一个数据。
- 优点:遍历内部数据时,可以按照添加的顺序去遍历,对应比较频繁的遍历操作LinkedHashSet性能高于HashSet。

---

## Set接口之->TreeSet

- 可以按照添加对象的指定属性进行排序

- 通过实现自然排序和订制排序来完成

---

## Map

- key:是无序的,不可重复的,使用Set存储所有的key,key所在的类要重写equals()和hashCode()(HashMap例)
- value:无序的,可重复的,使用Collection存储所有的value,value所在的类要重写equals()
- key和value构成了一个Entry对象。
- Entry:无序的,不可重复的,使用Set存储所有的entry
- map的实现类有HashMap，LinkedHashMap，TreeMap，Hashtable
- LinkedHashMap:可以按照添加时的顺序实现遍历。因为在HashMap底层结构基础上,添加了一对指针,指向前一个和后一个元素。对于频繁的遍历操作,此类执行效率高于HashMap。
- Hashtable:线程安全的,效率低;


## links
* [目录](<catelog.md>)
* 下一节: [多线程](resources/interview/thread.md)
