## 1.redis应用场景？

> - 分布式主键id，短信验证码，存储缓存信息
> - 利用hash的value自增自减实现抢购
> - 利用list的rpush什么的实现朋友圈点赞
> - set实现随机数据,网站访问量统计,黑名单,以及交并差及,共同好友啊
> - treeSet实现排行榜以及带有权重的任务队列	
> - 网站统计类,地址位置等

## 2.什么是Redis持久化？Redis有哪几种持久化方式？优缺点是什么？

> - 持久化就是把redis中的数据保存到磁盘,防止服务宕机造成的数据丢失。
>
>
> - RDB：
>
>   - 每过一段时间会把数据通过快照的形式保存到dump.rdb文件。
>   - 启动效率高,数据安全性低。
> - AOF：
>   - 将Redis中的写命令记录到日志中，从日志中恢复数据。
>   - 数据安全,日志多，恢复和启动速度慢。

## 3.缓存穿透

> - 就是你要访问的数据在缓存和数据库中都没有。
> - 我们可以把空结果进行缓存并设置短暂的过期时间来避免。

## 4.缓存击穿

> - 缓存过期了,大量线程请求到数据库中了。
> - 使用分布式锁redission保证只有一个请求能进来。

## 5.缓存雪崩

> - 缓存数据大批量过期了。造成的数据库压力过大
> - 设置热点数据永远不过期。别的数据随机过期时间,避免一起过期

## 6.Redis键的过期的删除策略

> - 定时过期：设置了过期时间的key过期了立即删除
>
> - 惰性过期：当访问一个key时判断是否过期，过期则删除。
>
> - 定期过期：定时扫描一部分key过期就删除
>

## 7.Redis的内存用完了会发生什么？

- 内存满了Redis的写命令会报错,读正常
- 可以配置内存淘汰机制，当Redis达到内存上限时会冲刷掉旧的内容。
- 全局的键和过期时间的键空间选择性移除


## links
* [目录](<catelog.md>)
* 下一节: [mysql](resources/interview/mysql.md)



