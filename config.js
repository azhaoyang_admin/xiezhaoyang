config.baseTitle = '开发随笔'
config.siteName = '开发随笔'
config.siteDes = '存放日常学习用的仓库'
config.siteKey = 'java-doc'

config.logoUrl = ''
//静态资源资源地址
config.assetBasePath = '/xiezhaoyang'
//图片资源地址
config.dataUrl = config.assetBasePath + '/resources/'

config.docProject = 'azhaoyang_admin/xiezhaoyang/wikis'
config.project = 'azhaoyang_admin/xiezhaoyang'
config.authorName = '朝阳'
config.authorPage = 'https://gitee.com/azhaoyang_admin/xiezhaoyang'
//语言设置
config.lang = 'zh-CN'
config.langs = ["en-US", "zh-CN"]
//主题
config.theme = 'paper'
config.codeTheme = 'github'
//左侧导航配置
config.catelogPage = 'catelog.md'
//默认展示页面
config.defaultPage = 'resources/01.0.md'
config.makeTOC = true
config.emptyData = 'No content to display!'

config.onContentWrited = function (contentDom) {
  // contentDom.find('h1').first().prev('h2').remove()
  contentDom.find('h1').first().prevAll().remove()
}
