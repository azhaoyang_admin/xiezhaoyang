## Java中有几种类型的流？

- 按照流的流向分为输入流和输出流(如果想从程序写东西到别的地方，那么就选择输入流，反之就选输出流)；

- 按数据单位分为字节流和字符流(每次传输一个字节选字节流(非文本文件),如果有中文选字符流了(文本文件))。

- 按照流的角色分为节点流和处理流。

---

## 字符流和字节流有什么区别？

- 字节流的操作不会经过缓冲区而是直接操作文本本身的，而字符流的操作会先经过缓冲区然后通过缓冲区再操作文件

- 字节流一般用来处理图像，视频，以及PPT，Word类型的文件。

- 字符流一般用于处理纯文本类型的文件，如TXT文件等。

- 字节流可以用来处理纯文本文件，但是字符流不能用于处理图像视频等非文本类型的文件。

## 什么是缓冲区？有什么作用？

- [ ] 缓冲区就是一段特殊的内存区域，很多情况下当程序需要频繁地操作一个资源（如文件或数据库）则性能会很低，所以为了提升性能就可以将一部分数据暂时读写到缓存区，以后直接从此区域中读写数据即可，这样就显著提升了性。
- [ ] 对于 Java 流的操作都是在缓冲区操作的，所以如果我们想在字符流操作中主动将缓冲区刷新到文件则可以使用 flush() 方法操作。

---

## 什么是Java序列化，如何实现Java序列化？

- 就是把对象转化为流的一种机制,可以对流化后的对象进行读写操作,也可将流化后的对象传输于网络之间。是为了解决对对象流进行读写操作时所引发的问题。

- java使用输入和输出流来实现序列化和反序列化。

- 实现Serializable,定义版本号。

- 使用文件输出流把新建的对象保存。序列化完成。

- 使用输入流把文件读取过来，使用强转，反序列化完成。

---

## serialVersionUID

- 用来保证在反序列时，发送方发送的和接受方接收的是可兼容的对象。

- 如果serialVersionUID不一致，会抛出 `InvalidClassException`。

- 如果在类中你没写,那么它的值是Java运行时根据类的内部细节自动生成的。

- 类的实例变量做了修改，serialVersionUID 可能发生变化。

- 不同的 jdk 编译也可能会生成不同的 `serialVersionUID` 默认值。

- 故建议，显式声明（写上把还是）。

---

## 默认序列化机制

- 在序列化对象时，不仅会序列化当前对象本身，还会对其父类的字段以及该对象引用的其它对象也进行序列化。父类的父类引用的引用。

- 父类和引用需要满足序列化条件被序列化的类必须属于 Enum、Array 和 Serializable 类型其中的任何一种


## links
* [目录](<catelog.md>)
* 下一节: [spring](resources/interview/spring.md)
