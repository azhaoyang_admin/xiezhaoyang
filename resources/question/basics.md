## 基本数据类型有哪些,String是不是基本数据类型 ?

- byte,int,short,long,Double,float,Boolean,Char。
- String声明为final的，不可被继承
- String实现了Serializable和Comparable接口：表示可以比较大小和实现系列化
- String内部定义了final char[] value用于存储字符串数据
- String:代表不可变的字符序列。
- 只要你对字符串的内容进行修改就得重新造,因为字符串常量池中是不会存储相同内容的字符串的
- 由于String是final的,所以在多线程中使用是安全的,我们不需要做任何其他同步操作。
- 因为java字符串是不可变的,可以在java运行时节省大量java堆空间。
- String是不可变类,每当我们对String进行操作的时候,总是会创建新的字符串。
- 操作String很耗资源,所以Java提供了两个工具类来操作StringBuffer和StringBuilder。
- StringBuffer和StringBuilder是可变类。
- StringBuffer是线程安全的,StringBuilder则不是线程安全的。

---

## 数据类型转换

- 当两个基本数据类型不一样时，会发生数据转换。
- 隐式转换(从小到大,自动处理),强制转换(从大到小)。

---

## int和Integer有什么区别

- Integer是int的包装类,int是基本数据类型;
- Integer变量必须实例化后才能使用,而int变量不需要;
- Integer实际是对象的引用,当new一个Integer时,实际上是生成一个指针指向此对象；而int则是直接存储数据值
- Integer的默认值是null，int的默认值是0

---

## ==和Equals区别

- equals属于Object类里的方法，没有重写默认是==
- 如果重写了equals我们往往比较的是对象中的属性是否相等
- ==比较基本数据类型，比较的是值

---

## switch-case 的穿透性

- 在switch语句中,如果case的后面不写break,将出现穿透现象,也就是不会在判断下一个case的值,

- 直接向后运行,直到遇到break,或者整体switch结束.

---

## 跳出流程语句

- break终止 switch或者循环(for) 
- continue结束本次循环，继续下一次的循环
- return其实也可以(当前方法返回什么值,退出当前方法)

---

## &和&&的区别

- &和&&都可以用作逻辑与的运算符，表示逻辑与（and）。
- &&具有短路的功能，而&不具备短路功能。
- 当&运算符两边的表达式的结果都为true时，整个运算结果才为true。
- 而&&运算符第一个表达式为false时，则结果为false，不再计算第二个表达式。

---

## 异常处理方式有几种

-- throws用在方法的声明上后接异常类名,是把异常抛给调用者
-- try...catch...finally捕获异常自己处理,处理完程序可以继续

---

## 常见的异常

- NullPointerException空指针异常

- ClassCastException （类转换异常）

- ArrayIndexOutOfBoundsException:数组索引越界异常

- ArithmeticException:算术运算异常

- NumberFormatException:数字格式异常

- IllegalArgumentException （非法参数异常）

- SecurityException （安全异常）

---

## Final、Finally、Finalize

- final:修饰符(类和变量和方法).

- finally:处理异常时候用,无论正常执行还是发生异常,只要JVM不关闭都能执行,一般用作释放资源

- finalize：Object类的方法,该方法是由垃圾回收机制在销毁对象时调用的,通过重写finalize() 方法可以整理系统资源或者执行其他清理工作。

---

## 什么是反射？

- 反射就是在运行时可以知道一个类的属性以及方法。

- 并且能够动态的创建对象，调用属性和方法对于私有属性也可以暴力破解

- JDBC反射加载数据库驱动。Spring的IOC

- 影响性能,反射是一种解释操作,我们需要告诉JVM怎么做。所以这类操作总是慢于直接执行java代码。

- 动态创建编译对象,体现了灵活性。通过反射机制我们可以获得类的各种内容,进行反编译。对于JAVA这种先编译再运行的语言来说，反射机制可以使代码更加灵活，更加容易实现面向对象。

---

## 枚举

- 当需要定义一组常量时，强烈建议使用枚举类，使用enum关键字来定义枚举类。

- 和普通 Java 类一样,枚举类可以实现一个或多个接口。

- 每个枚举值可以通过实现接口来展示不同的效果

---

## 注解(Annotation)

- Annotation就是代码里的特殊标记, 这些标记可以在编译, 类加载, 运行时被读取, 并执行相应的处理。

- 通过使用**Annotation**,程序员可以在不改变原有逻辑的情况下, 在源文件中嵌入一些补充信息。


## links
* [目录](<catelog.md>)
* 下一节: [面向对象](resources/interview/class.md)
