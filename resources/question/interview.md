## 12.你怎么防止优惠券有人重复刷？

对于重复请求，要考虑**接口幂等**和**接口防重**。

大家可以看下之前我写的这篇文章哈：[聊聊幂等设计](https://link.juejin.cn?target=https%3A%2F%2Fmp.weixin.qq.com%2Fs%3F__biz%3DMzg3NzU5NTIwNg%3D%3D%26mid%3D2247497427%26idx%3D1%26sn%3D2ed160c9917ad989eee1ac60d6122855%26chksm%3Dcf2229faf855a0ecf5eb34c7335acdf6420426490ee99fc2b602d54ff4ffcecfdab24eeab0a3%26token%3D529683793%26lang%3Dzh_CN%23rd)

防刷的话，可以限流以及加入黑名单处理。

- 为了防止某个用户请求优惠券过于频繁，我们可以对同一用户限流。
- 为了防止黄牛等模拟几个用户请求，我们可以对某个IP进行限流。
- 为了防止有人使用代理，每次请求都更换IP请求，我们可以对接口进行限流。

## 10. 聊聊分库分表,分表为什么要停服这种操作，如果不停服可以怎么做

## 10.1 分库分表方案

- 水平分库：以字段为依据，按照一定策略（hash、range等），将一个库中的数据拆分到多个库中。
- 水平分表：以字段为依据，按照一定策略（hash、range等），将一个表中的数据拆分到多个表中。
- 垂直分库：以表为依据，按照业务归属不同，将不同的表拆分到不同的库中。
- 垂直分表：以字段为依据，按照字段的活跃性，将表中字段拆到不同的表（主表和扩展表）中。

## 10.2 常用的分库分表中间件：

- sharding-jdbc（当当）
- Mycat
- TDDL（淘宝）
- Oceanus(58同城数据库中间件)
- vitess（谷歌开发的数据库中间件）
- Atlas(Qihoo 360)

## 10.3 分库分表可能遇到的问题

- 事务问题：需要用分布式事务啦
- 跨节点Join的问题：解决这一问题可以分两次查询实现
- 跨节点的count,order by,group by以及聚合函数问题：分别在各个节点上得到结果后在应用程序端进行合并。
- 数据迁移，容量规划，扩容等问题
- ID问题：数据库被切分后，不能再依赖数据库自身的主键生成机制啦，最简单可以考虑UUID
- 跨分片的排序分页问题（后台加大pagesize处理？）

## 10.4 分表要停服嘛？不停服怎么做？

不用。不停服的时候，应该怎么做呢，分五个步骤：

1. 编写代理层，加个开关（控制访问新的DAO还是老的DAO，或者是都访问），灰度期间，还是访问老的DAO。
2. 发版全量后，开启双写，既在旧表新增和修改，也在新表新增和修改。日志或者临时表记下新表ID起始值，旧表中小于这个值的数据就是存量数据，这批数据就是要迁移的。
3. 通过脚本把旧表的存量数据写入新表。
4. 停读旧表改读新表，此时新表已经承载了所有读写业务，但是这时候不要立刻停写旧表，需要保持双写一段时间。
5. 当读写新表一段时间之后，如果没有业务问题，就可以停写旧表啦

